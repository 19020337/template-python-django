from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from rest_framework.parsers import JSONParser

def list(request):
    return JsonResponse({"status": "OK", "data": [1, 2, 3]})

def students(request, id: int):
    if request.method == PUT:
        return put(request, id)
    else:
        return get(request, id)
        
def put(request, id: int):
    return JsonResponse({"status": "OK", "data": JSONParser().parse(request)})

def get(request, id: int):
    return JsonResponse({"status": "OK", "data": {"masv" : id}})

def home(request):
    return HttpResponse("""
    <a href=\"/get\">/get</a><br>
    <a href=\"/list\">/list</a><br>
    <a href=\"/update\">/update</a><br>
    """)

def get_view(request):
    return render(request, 'get.htm')
def list_view(request):
    return render(request, 'list.htm')
def update_view(request):
    return render(request, 'update.htm')